package com.caseStudy.eCart.Service;

import com.caseStudy.eCart.Repository.CartRepository;
import com.caseStudy.eCart.Repository.OrderRepository;
import com.caseStudy.eCart.Repository.ProductRepository;
import com.caseStudy.eCart.Repository.UserRepository;
import com.caseStudy.eCart.model.Cart;
import com.caseStudy.eCart.model.Order;
import com.caseStudy.eCart.model.Product;
import com.caseStudy.eCart.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CartService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CartRepository cartRepository;
    //@Autowired
    //private FixedCartRepository fixedCartRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OrderRepository orderRepository;

    public Cart addProduct(int userid,Long productid)
    {
        Product products=productRepository.findById(productid).get();
        Users user=userRepository.findById(userid).get();
        if(cartRepository.findByUserAndProducts(user,products).isPresent())
        {
            Cart car=cartRepository.findByUserAndProducts(user,products).get();
            //FixedCart fixedCart=fixedCartRepository.findByRefid(car.getId().intValue());

            car.setQuantity(car.getQuantity()+1);
            //fixedCart.setQuantity(fixedCart.getQuantity()+1);
            cartRepository.save(car);
            //fixedCartRepository.save(fixedCart);
        }
        else
        {
            Cart c=new Cart(products,user,1);
            //fixedCart fc=new fixedCart(product,user,1);
            cartRepository.save(c);
            //fixedCartRepository.save(fc);
        }
        return cartRepository.findByUserAndProducts(user,products).get();
    }
    public Cart removeproduct(int userid,Long productid)
    {
        Product products=productRepository.findById(productid).get();
        Users user=userRepository.findById(userid).get();
            Cart car=cartRepository.findByUserAndProducts(user,products).get();
            //FixedCart fixedCart=fixedCartRepository.findByRefid(car.getId().intValue());
                car.setQuantity(car.getQuantity() - 1);
                if(car.getQuantity()==0)
                {
                    Cart cart=cartRepository.findByUserAndProducts(user,products).get();
                    cartRepository.delete(cart);
                    return cart;
                }
                else {
                    //fixedCart.setQuantity(fixedCart.getQuantity()+1);
                    cartRepository.save(car);
                    return cartRepository.findByUserAndProducts(user,products).get();
                }
            //fixedCartRepository.save(fixedCart);
        //return cartRepository.findByUserAndProducts(user,products).get();
    }
    public String addtocart(int userid,Long productid)
    {
        Product products=productRepository.findById(productid).get();
        Users user=userRepository.findById(userid).get();
        if(cartRepository.findByUserAndProducts(user,products).isPresent())
        {
            return "\"This product is already present in your cart\"";
        }
        else
        {
            Cart cart=new Cart();
            cart.setProducts(products);
            cart.setUser(user);
            cart.setQuantity(1);
            cartRepository.save(cart);
        }
        return "\"product added\"";
    }
    public String removefromcart(int userid,Long productid)
    {
        Product products=productRepository.findById(productid).get();
        Users user=userRepository.findById(userid).get();
        Cart cart=cartRepository.findByUserAndProducts(user,products).get();
        cartRepository.delete(cart);
        return "\"product removed\"";
    }
    /*public Optional<Users> CurrentUser(Principal principal){
        String email=principal.getName();
        return userRepository.findByEmail(email);
    }
    public Long getuserid(Principal principal)
    {
        String email=principal.getName();
        Long id=userRepository.findByEmail(email).get().getUserid();
        return id;
    }
    public Long getuserrole(Principal principal)
    {
        return userRepository.findByEmail(principal.getName()).get().getRole().getRoleid();
    }
    public Optional<Users> getuserprofile(Principal principal)
    {
        return userRepository.findByEmail(principal.getName());
    }
    public ResponseEntity<?> checkdetails(Users user,Principal principal)
    {
        Optional<Users> usercheck=userRepository.findByEmail(principal.getName());
        Optional<Users> usercheckinfo=userRepository.findByEmail(user.getEmail());
        if(usercheckinfo.isPresent() & usercheckinfo.get().getEmail() != usercheck.get().getEmail()){
            HttpHeaders responseHeaders=new HttpHeaders();
        }
        else{
            Cart cart=new Cart();
            cart.setItems(product);
            cart.setUser(user);
            cart.setQuantity(1);
            cartRepsoitory.save(cart);
        }
    }*/
    public List<Cart> showcart(int userid)
    {
        Users user=userRepository.findById(userid).get();
        return cartRepository.findByUser(user);
    }
    public String clearcart(int userid) {
        Users user=userRepository.findById(userid).get();
        List<Cart> cartitems=cartRepository.findAllByUser(user);
        for(Cart cart:cartitems)
        {
            cartRepository.deleteById(cart.getId());
        }
        return "\"cart cleared\"";
    }
    public List<Order> checkout(Principal principal)
    {
        Users users=userRepository.findByEmail(principal.getName()).get();
        List<Cart> cartList=cartRepository.findAllByUser(users);
        for(Cart cart:cartList)
        {
            Order order=new Order();
            order.setUserid(cart.getUser().getId());
            order.setQuantity(cart.getQuantity());
            order.setPrice(cart.getProducts().getPrice());
            order.setProductName(cart.getProducts().getName());
            order.setDate(new Date());
            cartRepository.delete(cart);
            orderRepository.saveAndFlush(order);
        }
        return orderRepository.findAllByUserid(users.getId());
    }
}
