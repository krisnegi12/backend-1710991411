package com.caseStudy.eCart.Service;

import com.caseStudy.eCart.Repository.ProductRepository;
import com.caseStudy.eCart.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Optional<Product> findProductById(Long productId) {
        return productRepository.findById(productId);
    }
}
