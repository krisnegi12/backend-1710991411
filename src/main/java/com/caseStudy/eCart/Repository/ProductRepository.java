package com.caseStudy.eCart.Repository;

import com.caseStudy.eCart.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long>{

    List<Product> findByCategory(String category);
    List<Product> findAllByCategoryAndPriceBetween(String category, int p1, int p2);
    List<Product> findAllByPriceBetween(int p1,int p2);
    List<Product> findByName(String name);
    //Product findByproductid(Long id);
}
