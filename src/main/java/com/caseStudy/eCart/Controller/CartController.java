package com.caseStudy.eCart.Controller;

import com.caseStudy.eCart.Service.CartService;
import com.caseStudy.eCart.Service.CurrentUserService;
import com.caseStudy.eCart.model.Cart;
import com.caseStudy.eCart.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value="/api")
@CrossOrigin(origins = "http://localhost:4200")
public class CartController {
    //@Autowired
    private CartService cartService;
    //@Autowired
    private CurrentUserService currentUserService;

    @Autowired
    public CartController(CartService cartService,CurrentUserService currentUserService){
        this.cartService=cartService;
        this.currentUserService=currentUserService;
    }

    @RequestMapping(value="/addproduct/recieve/{productid}",method= RequestMethod.GET)
    @ResponseBody
    public Cart addproduct(@PathVariable Long productid, Principal principal){
        return cartService.addProduct(currentUserService.getuserid(principal),productid);
    }
    @RequestMapping(value="/removeproduct/recieve/{productid}",method= RequestMethod.GET)
    @ResponseBody
    public Cart removeproduct(@PathVariable Long productid, Principal principal){
        return cartService.removeproduct(currentUserService.getuserid(principal),productid);
    }
    @RequestMapping(value="/addtocart/recieve/{productid}",method= RequestMethod.GET)
    @ResponseBody
    public String addtocart(@PathVariable Long productid, Principal principal){
        return cartService.addtocart(currentUserService.getuserid(principal),productid);
    }
    @RequestMapping(value="/removefromcart/recieve/{productid}",method= RequestMethod.GET)
    @ResponseBody
    public String removefromcart(@PathVariable Long productid, Principal principal){
        return cartService.removefromcart(currentUserService.getuserid(principal),productid);
    }
    @RequestMapping(value="/showcart/recieve",method= RequestMethod.GET)
    @ResponseBody
    public List<Cart> showcart(Principal principal){
        return cartService.showcart(currentUserService.getuserid(principal));
    }
    @RequestMapping(value="/checkout/recieve",method= RequestMethod.GET)
    @ResponseBody
    public List<Order> checkout(Principal principal){
        return cartService.checkout(principal);
    }
    @RequestMapping(value="/clearcart/recieve",method= RequestMethod.GET)
    @ResponseBody
    public String clearcart(Principal principal){
        return cartService.clearcart(currentUserService.getuserid(principal));
    }
}
