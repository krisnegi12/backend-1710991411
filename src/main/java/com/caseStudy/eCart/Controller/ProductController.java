package com.caseStudy.eCart.Controller;

import com.caseStudy.eCart.Service.ProductService;
import com.caseStudy.eCart.model.Product;
import com.caseStudy.eCart.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductService productService;
    @GetMapping("/getAllProducts")
    public List<Product> getAllProducts()
    {
        return productRepository.findAll();
    }
    @PostMapping("/createProducts")
    public Product createProduct(@Valid @RequestBody Product product) {
        product.setImg("./assets/newprod.jpg");
        return productRepository.save(product);
    }
    @GetMapping("/products/{id}")
    public Optional<Product> getNoteById(@PathVariable(value = "id") Long productId) {
        return productService.findProductById(productId);

    }
    @GetMapping("/products/category/{category}")
    public List<Product> getNodeByCategory(@PathVariable(value="category")String category)
    {
        return productRepository.findByCategory(category);
    }
    @GetMapping("/products/{category}/{p1}/{p2}")
    public List<Product> getNodeByPriceRange(@PathVariable(value="category")String category,@PathVariable(value="p1")int p1,@PathVariable(value="p2")int p2)
    {
        return productRepository.findAllByCategoryAndPriceBetween(category,p1,p2);
    }
    @GetMapping("/products/{p1}/{p2}")
    public List<Product> getNodeByAllPrice(@PathVariable(value="p1")int p1,@PathVariable(value="p2")int p2)
    {
        return productRepository.findAllByPriceBetween(p1,p2);
    }
    @GetMapping("/products/name/{name}")
    public List<Product> getNoteByName(@PathVariable(value = "name") String productName) {
        return productRepository.findByName(productName);

    }
    @PutMapping("/editproduct/{id}")
    public Product editProduct(@PathVariable(value = "id") Long productId,@Valid @RequestBody Product newproduct)
    {
        Product prod=productRepository.findById(productId).get();
        prod.setName(newproduct.getName());
        prod.setPrice(newproduct.getPrice());
        prod.setCategory(newproduct.getCategory());
        prod.setDetails(newproduct.getDetails());
        return productRepository.save(prod);
    }
}
