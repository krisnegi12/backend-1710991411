package com.caseStudy.eCart.Controller;

import com.caseStudy.eCart.Repository.OrderRepository;
import com.caseStudy.eCart.Repository.UserRepository;
import com.caseStudy.eCart.model.Order;
import com.caseStudy.eCart.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value="/api")
@CrossOrigin(origins = "http://localhost:4200")
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value="/showorder",method= RequestMethod.GET)
    @ResponseBody
    public List<Order> showOrder(Principal principal)
    {
        Users users=userRepository.findByEmail(principal.getName()).get();
        return orderRepository.findAllByUserid(users.getId());
    }
}
