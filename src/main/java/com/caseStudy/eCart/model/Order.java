package com.caseStudy.eCart.model;

//import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
//import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name="orderhistory")
public class Order implements Serializable {
    @Column(name="id")
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @Column(name="productName")
    @NotNull
    private String productName;
    @Column(name="date")
    @NotNull
    /*@Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern="dd-MM-yyyy'T'HH:mm:ss")*/
    private Date date;
    @Column(name="userid")
    @NotNull
    private int userid;
    @Column(name="price")
    @NotNull
    private int price;
    @Column(name="quantity")
    @NotNull
    private int quantity;

    public Order(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
